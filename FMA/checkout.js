﻿window.onbeforeunload = function () {}		// Stops caching of pages
var ShoppingArea = ShoppingArea || {}; 		// NameSpace declaration
//==================================================================================
// DIV CREATION & HTML POPULATING WITH JSON INVENTORY DATA FOR INVENTORY PAGE
//==================================================================================
function addItemsToPage() {
	for( var i = 0; i < inventory.item.length; ++i ) {
		cat = inventory.item[ i ].category;
		createItemDiv( 'parentContainer', 'itemContainer', inventory.item[ i ].id, cat, '' );
		createItemDiv( 'image', 'image', inventory.item[ i ].id, cat, inventory.item[ i ].image );
		createItemDiv( 'buyInterface', 'buyInterface', inventory.item[ i ].id, cat,	inventory.item[ i ].price );
		createItemDiv( 'text', 'name', inventory.item[ i ].id, cat, inventory.item[ i ].name );
		createItemDiv( 'text', 'description', inventory.item[ i ].id, cat, inventory.item[ i ].description );
	}
	document.getElementById( 'products' ).removeChild( document.getElementById( 'unsupported' ) );
}
function createItemDiv( type, className, id, category, content ) {
	var parentContainerId = id + 'box';
	if( type == 'parentContainer' ) {
		var div = document.createElement( 'div' );
		div.setAttribute( 'id', parentContainerId );
		div.setAttribute( 'class', className );
		document.getElementById( category ).appendChild( div ); 
	} else if( type == 'text' || type == 'image' || type == 'buyInterface' ) {
		var div = document.createElement( 'div' );
		document.getElementById( parentContainerId ).appendChild( div );
		div.setAttribute( 'class', className );		
		if( type == 'text' ) {
			div.innerHTML = content;
		} else if( type == 'image' ) {
			div.innerHTML = "<img src='images/" + content + "' alt='" + content + "' height='100' width='100'>";
		} else if( type == 'buyInterface' ) {
			var divId = 'buy' + id;
			var feedbackId = 'talk' + id;
			div.innerHTML += "<span class='price'>&#163; " + content + "</span>";
			div.innerHTML += "</br><input class='addAmount' id='" + divId + "' type='text' maxlength='2' value='1' onfocus='clearThisField( this )'/>";
			div.innerHTML += "<input class='addButton' type='button' value='Add' onclick='ShoppingArea.addToCart( \"" + divId + "\" )' />";
			div.innerHTML += "</br><span id='" + feedbackId + "'></span>"; 
		}
	}
}
//==================================================================================
// SHOPPING CART, ITEM AND CUSTOMER CLASSES WITH THEIR METHODS AND SHOPPING-AREA VARS
//==================================================================================
ShoppingArea.tax = 17.5;
ShoppingArea.discountRate = 0.125;
ShoppingArea.discountPoint = 6;
ShoppingArea.currentPage;
//---------------------------------------------
// SHOPPING CART CLASS
//---------------------------------------------
ShoppingArea.ShoppingCart = function() {
	this.chronoStamp;
	this.isEmpty = true;
	this.content = new Array();
	this.subTotal1 = 0;	//includes VAT
	this.unitsTotal = 0;
	this.taxTotal = 0;
	this.discountTotal = 0;
	this.subTotal2 = 0;
	this.shippingSelected = 0;
	this.shippingName = '';
	this.shippingCost = 0;
	this.total = 0;
	this.healthyOption = false;
	//---------------------------------------------
	// Adds item to basket
	//---------------------------------------------
	this.addItem = function( id, units ) {
		//console.log( "OBJECT FUNCTION <ShoppingCart.addItem> for: " + id );
		var size = this.content.length;
		var location = whereIsItem( this.content, id );
		if( size == 0 || location < 0 ) {
			this.content[ this.content.length ] = new ShoppingArea.Item( id );
			location = size;
		}
		this.content[ location ].add( units );
		this.isEmpty = false;
		this.calculateCart();
	}
	//---------------------------------------------
	// Removes item(s) from basket
	//---------------------------------------------
	this.removeItem = function( id, parameter ) {
		//console.log( "OBJECT FUNCTION <ShoppingCart.removeItem> " + parameter + " " + id );
		var location = whereIsItem( this.content, id );
		if( this.content.length > 0 && location >= 0 ) {
			if( parameter == 'single' ) {
				if( this.content[ location ].itsUnits > 1 ) {
					this.content[ location ].remove();
				} else {
					this.content.splice( location, 1 ); //Suport IE4+, Moz4+
					this.isEmpty = true;
				}
			}
			else if( parameter == 'all' ) {
				this.content.splice( location, 1 ); //Suport IE4+, Moz4+
				this.isEmpty = true;
			}
		}
		this.calculateCart();
	}
	//---------------------------------------------
	// Clears ShoppingCart
	//---------------------------------------------
	this.removeAll = function( id ) {
		//console.log( "OBJECT FUNCTION <ShoppingCart.removeAll> " );
		this.isEmpty = true;
		this.content.splice( 0, this.content.length ); 
		this.calculateCart();
		this.shippingSelected = 0;
		this.shippingName = '';
		this.shippingCost = 0;
	}
	//---------------------------------------------
	// Checks where item is in the basket (-1 for not found)
	//---------------------------------------------
	function whereIsItem( content, id ) {
		//console.log( "OBJECT FUNCTION <whereIsItem>" );
		for( var i = 0; i < content.length; ++i ) {
			if( content[ i ].itsId == id ) {
				return i;
			}
		}
		return -1;
	}
	//---------------------------------------------
	// Calculate 1st subtotal, vat, discount, 2nd subtotal
	//---------------------------------------------
	this.calculateCart = function() {
		//console.log( "OBJECT FUNCTION <ShoppingCart.calculateCart>" );
		this.subTotal1 = 0;
		this.unitsTotal = 0;
		this.taxTotal = 0;
		this.discountTotal = 0;
		this.subTotal2 = 0;
		for( var i = 0; i < this.content.length; ++i ) {
			this.subTotal1 += this.content[ i ].itsTotal;
			this.unitsTotal += this.content[ i ].itsUnits;
			this.taxTotal = this.subTotal1 / ( 100 + ShoppingArea.tax ) * ShoppingArea.tax;
			if( this.unitsTotal >= ShoppingArea.discountPoint ) {
				this.discountTotal = this.subTotal1 * ShoppingArea.discountRate;
			}
			this.subTotal2 = this.subTotal1 - this.discountTotal;
			this.total = parseFloat( this.subTotal2 + this.shippingCost );
		}
	}
	//---------------------------------------------
	// Gets shipping info from JSON and sets it in Basket.
	//---------------------------------------------
	this.setShipping = function( index ) {
		//console.log( "OBJECT FUNCTION <ShoppingCart.setShipping> for: " + index );		
		if( index > 0 ) {
			if( index <= shipping.options.length ) {
				this.shippingSelected = index;
				this.shippingName = shipping.options[ ( index - 1 ) ].name;
			} else {
				//console.log( "ERROR <ShoppingArea.applyShipping> : select value bigger than JSON shipping length" ); 
			}
		}
	}
	//---------------------------------------------
	// Gets and sets shipping costs
	//---------------------------------------------
	this.getShipping = function() {
		//console.log( "OBJECT FUNCTION <ShoppingCart.getShipping>" );		
		if( this.shippingSelected > 0 ) { 
			if( this.unitsTotal < shipping.options[ this.shippingSelected - 1 ].discountPoint ) {
				this.shippingCost = parseFloat( shipping.options[ this.shippingSelected - 1 ].stdPrice );
			} else {
				this.shippingCost = parseFloat(shipping.options[ this.shippingSelected - 1 ].discountPrice );
			}
			this.calculateCart();
		}
	}
};
//---------------------------------------------
// ITEM CLASS
//---------------------------------------------
ShoppingArea.Item = function( id ) {
	this.itsId = id;
	this.itsName = fetchName( this.itsId );
	this.itsPrice = fetchPrice( this.itsId );
	this.itsUnits = 0;
	this.itsTotal = 0;
	//---------------------------------------------
	// Adds specified amount of units
	//---------------------------------------------	
	this.add = function( units ) {
		//console.log( "OBJECT FUNCTION <Item.add> "+ this.itsId );
		this.itsUnits += parseInt( units );
		this.itsTotal = this.itsPrice * this.itsUnits;
	}
	//---------------------------------------------
	// Removes a single unit
	//---------------------------------------------	
	this.remove = function() {
		//console.log( "OBJECT FUNCTION <Item.remove>" );
		if( this.itsUnits > 1 ) {
			this.itsUnits--;
			this.itsTotal = this.itsUnits * this.itsPrice;
		}
	}
	//---------------------------------------------
	// Fetches the price of item from JSON Inventory
	//---------------------------------------------		
	function fetchPrice( itsId ) {
		for( var i = 0; i < inventory.item.length; ++i ) {
			if( inventory.item[ i ].id == itsId ) {
				//console.log( "OBJECT FUNCTION <fetchPrice> for " + itsId + " found: " + inventory.item[ i ].price );
				return inventory.item[ i ].price;	
			}
		}
		//console.log( "ERROR <fetchPrice> ID: " + itsId );
		return -1;
	}
	//---------------------------------------------
	// Fetches the name of item from JSON Inventory
	//---------------------------------------------			
	function fetchName( itsId ) {
		for( var i = 0; i < inventory.item.length; ++i ) {
			if( inventory.item[ i ].id == itsId ) {
				return inventory.item[ i ].name;	
			}
		}
		//console.log( "ERROR <fetchName> ID: " + itsId );
		return "unknown";		
	}	
};
//---------------------------------------------
// CUSTOMER CLASS
//---------------------------------------------
ShoppingArea.Customer = function() {
	this.isEmpty = true;
	this.sameAddress = false;
	/* ADDRESS ARRAYS ORDER: 0:title, 1:name, 2:surname, 3:address1, 4:address2, 5:postcode, 6:city, 7:country ( mirrors the order on the form ) */
	this.billingAddress = new Array( 0, '', '', '', '', '', '', shipping.defaultDestination );
	this.shippingAddress = new Array( 0, '', '', '', '', '', '', shipping.defaultDestination );
	this.email = '';
	this.phone = '';
	//---------------------------------------------
	// Copies out billing address to shipping address if same
	//---------------------------------------------	
	this.copyAddress = function() {
		//console.log( "OBJECT FUNCTION: <Customer.copyAddress>" );
		if( this.sameAddress == true ) {
			for( var i = 0; i < this.billingAddress.length; ++i ) {
				this.shippingAddress[ i ] = this.billingAddress[ i ];
			}
		}
	}
	//---------------------------------------------
	// Clears out shipping address
	//---------------------------------------------	
	this.clearShippingAddress = function() {
		//console.log( "OBJECT FUNCTION: <Customer.clearShippingAddress>" );
		if( this.sameAddress == false ) {
			this.shippingAddress = new Array( 0, '', '', '', '', '', '', shipping.defaultDestination );
		}
	}
}
//==================================================================================
// LOADERS & DATA SAVING/LOADING (-->COOKIE)
//==================================================================================
function goBack() { //===Navigates back to the previous page===
	//console.log( "FUNCTION <goBack> in page " + ShoppingArea.currentPage );
	switch( ShoppingArea.currentPage ) {
		case 2:		window.location = "index.html";
						break;
		case 3:		window.location = "checkout1.html";
						break;
		case 4:		window.location = "checkout2.html";
						break;
		case 5: 	window.location = "checkout3.html";
						break;
		default: 	window.location = "index.html";
	}
}
function loadShopping() { //===Loads the shopping page===
	//console.log( "FUNCTION <loadShopping>");
	ShoppingArea.currentPage = 1;
	if( browserCheck() == false ) {
		document.getElementById( 'unsupported' ).innerHTML = "Your browser is unsupported. We officially support Firefox 12.0+, Internet Explorer 8.0+, Chrome 22.0+.";
	} else if( checkCookie() == false ) {
		document.getElementById( 'unsupported' ).innerHTML = "Cookies are disabled or blocked. Please enable them then refresh the page";
	} else {
		addItemsToPage(); //populate with inventory
		ShoppingArea.Basket = new ShoppingArea.ShoppingCart(); //basket object
		ShoppingArea.loadCookieToCart();
		ShoppingArea.refreshAll();
	}
}
function goToCheckout() { //===Navigates to the 1st checkout page ( cart, shipping )===
	if( ShoppingArea.validatePage() == true ) {
		window.location = "checkout1.html";
	}
}
function loadCheckout() { //===Loads the 1st checkout page ( cart, shipping )===
	//console.log( "FUNCTION <loadCheckout>");
	ShoppingArea.currentPage = 2;
	ShoppingArea.Basket = new ShoppingArea.ShoppingCart(); //basket object
	ShoppingArea.loadCookieToCart();
	if( ShoppingArea.Basket.isEmpty == true ) {
		//console.log( "Empty Basket, going back" );
		window.location = "shop.html";
	} else {
		ShoppingArea.refreshAll();
	}
}
function goToCheckout2() { //===Navigates to the 2nd checkout page ( address )===
	if( ShoppingArea.validatePage() == true ) {
		window.location = "checkout2.html";
	} 
}
function loadCheckout2() { //===Loads the 2nd checkout page ( address )===
	//console.log( "FUNCTION <loadCheckout2>");
	ShoppingArea.currentPage = 3;
	ShoppingArea.Basket = new ShoppingArea.ShoppingCart(); //basket object
	ShoppingArea.loadCookieToCart();
	if( ShoppingArea.Basket.isEmpty == true ) {
		//console.log( "Empty Basket, going back" );
		window.location = "shop.html";
	} else if( ShoppingArea.Basket.shippingSelected == 0 ) {
		//console.log( "No shipping selected, going back" );
		window.location = "checkout1.html";
	} else {
		ShoppingArea.addSelectFieldsCHK2();
		ShoppingArea.Buyer = new ShoppingArea.Customer(); //customer object
		ShoppingArea.loadCookieToCustomer();
		ShoppingArea.refreshAll();
	}
}
function doubleAddress() { //===Copy billing info to shipping info / Clears shipping fields=== 
	//console.log( "FUNCTION <doubleAddress>");
	var  chkbox = document.getElementById( 'sameAddress' ).checked;
	var billingFields = new Array( 'titleSelectorB', 'nameB', 'surnameB', 'address1B', 'address2B', 'postcodeB', 'cityB', 'countrySelectorB' );
	var shippingFields = new Array( 'titleSelectorS', 'nameS', 'surnameS', 'address1S', 'address2S', 'postcodeS', 'cityS', 'countrySelectorS' );
	if( chkbox == true ) {
		ShoppingArea.Buyer.sameAddress = true;
		for( var i = 0; i < billingFields.length; ++i ) {
			document.getElementById( shippingFields[ i ] ).value = document.getElementById( billingFields[ i ] ).value;
			document.getElementById( shippingFields[ i ] ).disabled = true;
		}
	} else {
		ShoppingArea.Buyer.sameAddress = false;
		ShoppingArea.Buyer.clearShippingAddress();
		for( var i = 0; i < shippingFields.length; ++i ) {
			if( i == 0 ) { //for 'titleSelectorX' select box
				document.getElementById( shippingFields[ i ] ).value = 0;
			} else if( i == 7 ) { //for 'coutrySelectorX' select box
				document.getElementById( shippingFields[ i ] ).value = shipping.defaultDestination;
			} else { //for everything else
				document.getElementById( shippingFields[ i ] ).value = '';
			}
			document.getElementById( shippingFields[ i ] ).disabled = false;
		}
	}
}
function doubleUp( boxID ) { //===if [sameAddress] is checked: copies out as-you-type billing info to shipping fields
	if( document.getElementById( 'sameAddress' ).checked ==true ) {
		var mirrorID = boxID.replace( 'B', 'S' );
		document.getElementById( mirrorID ).value = document.getElementById( boxID ).value;
	}
}
function goToCheckout3() { //===Navigates to the 3rd checkout page ( Confirmation )===
	if( ShoppingArea.validatePage() == true ) {
		ShoppingArea.saveCustomer();
		ShoppingArea.saveCustomerToCookie();
		window.location = "checkout3.html";
	}
}
function loadCheckout3() { //===Loads the 3rd checkout page ( Confirmation )===
	//console.log( "FUNCTION <loadCheckout3>");
	ShoppingArea.currentPage = 4;
	ShoppingArea.Basket = new ShoppingArea.ShoppingCart(); //basket object
	ShoppingArea.loadCookieToCart();
	ShoppingArea.Buyer = new ShoppingArea.Customer(); //customer object
	ShoppingArea.loadCookieToCustomer();
	if( ShoppingArea.Basket.isEmpty == true ) {
		//console.log( "Empty Basket, going back" );
		window.location = "shop.html";
	} else if( ShoppingArea.Basket.shippingSelected == 0 ) {
		//console.log( "No shipping selected, going back" );
		window.location = "checkout1.html";
	}else if( ShoppingArea.Buyer.isEmpty == true ) {
		//console.log( "No customer details, going back" );
		window.location = "checkout2.html";
	} else {
		ShoppingArea.refreshAll();
	}	
}
function goToCheckout4() { //===Navigates to the 4th checkout page ( Payment )===
	if( ShoppingArea.validatePage() == true ) {
		window.location = "checkout4.html";
	}
}
function loadCheckout4() { //===Load the 4th checkout page ( Payment )===
	//console.log( "FUNCTION <loadCheckout4>");
	ShoppingArea.currentPage = 5;
	ShoppingArea.Basket = new ShoppingArea.ShoppingCart(); //basket object
	ShoppingArea.loadCookieToCart();
	ShoppingArea.Buyer = new ShoppingArea.Customer(); //customer object
	ShoppingArea.loadCookieToCustomer();
	if( ShoppingArea.Basket.isEmpty == true ) {
		//console.log( "Empty Basket, going back" );
		window.location = "shop.html";
	} else if( ShoppingArea.Basket.shippingSelected == 0 ) {
		//console.log( "No shipping selected, going back" );
		window.location = "checkout1.html";
	}else if( ShoppingArea.Buyer.isEmpty == true ) {
		//console.log( "No customer details, going back" );
		window.location = "checkout2.html";
	} else {
		ShoppingArea.refreshAll();
	}
}
ShoppingArea.pay = function() { //===Sends all info to server on SUBMIT===
	//console.log( "FUNCTION <ShoppingArea.pay>" );
	if( ShoppingArea.validatePage() == true ) {
		// Time&Date stamp creation for the order
		var currentDate = new Date();
		var dateTimeStamp = currentDate.getDate() + '/' + ( currentDate.getMonth() + 1 ) + '/' + currentDate.getFullYear() + '-' + currentDate.getHours() + ':' + currentDate.getMinutes();
		ShoppingArea.Basket.chronoStamp = dateTimeStamp;
		// Add Basket and Customer to form for sending
		document.getElementById( 'Basket' ).value =  JSON.stringify( ShoppingArea.Basket );
		document.getElementById( 'Customer' ).value = JSON.stringify( ShoppingArea.Buyer );
		killCookie( 'YumJuiceCustomer' );
		killCookie( 'YumJuiceCart' );
		document.getElementById( 'ORDER' ).submit();
	}
}
ShoppingArea.loadCookieToCart = function() { //===Rebuilds the basket with data from cookie===
	//console.log( "FUNCTION <ShoppingArea.loadCookieToCart>");
	ShoppingArea.Basket.removeAll();
	var rawData = readCookie( 'YumJuiceCart' );
	if( rawData != 'invalid' ) {
		var cookedData = JSON.parse( rawData );
		for( var i = 0; i < cookedData.content.length; ++i ) {
			var id = cookedData.content[ i ].itsId;
			var units = cookedData.content[ i ].itsUnits;
			ShoppingArea.Basket.addItem( id, units );
		}
		ShoppingArea.Basket.setShipping( cookedData.shippingSelected );
		ShoppingArea.Basket.healthyOption = cookedData.healthyOption;
	}
}
ShoppingArea.saveCartToCookie = function() { //===Saves the basket to a cookie===
	//console.log( "FUNCTION <ShoppingCart.saveCartToCookie>");	
	var s = JSON.stringify( ShoppingArea.Basket );
	createCookie( 'YumJuiceCart', s );
}
ShoppingArea.loadCookieToCustomer = function() { //===Rebuilds the customer details with the data of the cookie===
	//console.log( "FUNCTION <ShoppingArea.loadCookieToCustomer>" );
	var rawData = readCookie( 'YumJuiceCustomer' );
	if( rawData != 'invalid' ) {
		var cookedData = JSON.parse( rawData );
		if( ShoppingArea.Buyer.billingAddress.length != cookedData.billingAddress.length || ShoppingArea.Buyer.shippingAddress.length != cookedData.shippingAddress.length ) {
			//console.log( "<ShoppingCart.loadCookieToCustomer> : mismatched array length" );
		} else {
			for( var i = 0; i < ShoppingArea.Buyer.billingAddress.length; ++i ) {
				ShoppingArea.Buyer.billingAddress[ i ] = cookedData.billingAddress[ i ];
				ShoppingArea.Buyer.shippingAddress[ i ] = cookedData.shippingAddress[ i ];
			}
			ShoppingArea.Buyer.isEmpty = cookedData.isEmpty;
			ShoppingArea.Buyer.sameAddress = cookedData.sameAddress;
			ShoppingArea.Buyer.email = cookedData.email;
			ShoppingArea.Buyer.phone = cookedData.phone;
		}
	}
}
ShoppingArea.saveCustomer = function() { //===Grabs the field values from address page and saves them into the object===
	//console.log( "FUNCTION <ShoppingCart.saveCustomer>");
	var billingFields = new Array( 'titleSelectorB', 'nameB', 'surnameB', 'address1B', 'address2B', 'postcodeB', 'cityB', 'countrySelectorB' );
	var shippingFields = new Array( 'titleSelectorS', 'nameS', 'surnameS', 'address1S', 'address2S', 'postcodeS', 'cityS', 'countrySelectorS' );
	for( var i = 0; i < billingFields.length; ++i ) {
		ShoppingArea.Buyer.billingAddress[ i ] = document.getElementById( billingFields[ i ] ).value;
	}
	if( ShoppingArea.Buyer.sameAddress == true ) {
		ShoppingArea.Buyer.copyAddress();
	} else {
		for( var i = 0; i < shippingFields.length; ++i ) {
			ShoppingArea.Buyer.shippingAddress[ i ] = document.getElementById( shippingFields[ i ] ).value;
		}
	}		
	ShoppingArea.Buyer.email = document.getElementById( 'email' ).value;
	ShoppingArea.Buyer.phone = document.getElementById( 'phone' ).value;
	ShoppingArea.Buyer.isEmpty = false;
}
ShoppingArea.saveCustomerToCookie = function() { //===Saves the customer's details to a cookie===
	//console.log( "FUNCTION <ShoppingCart.saveCustomerToCookie>");
	var s = JSON.stringify( ShoppingArea.Buyer );
	createCookie( 'YumJuiceCustomer', s );
}
//==================================================================================
// INVENTORY AND SHOPPING CART INTERFACE FOR THE CLASSES
//==================================================================================
function clearThisField( id ) {	//===Clear an input field on demand===
	id.value = '';
}
ShoppingArea.addToCart = function( buyID ) { //===Checks input then calls shopping basket's <addItem>===
	if( document.getElementById( buyID ).value == '' ) {
		document.getElementById( buyID ).value = 1;
	}
	var units = document.getElementById( buyID ).value;
	var id = buyID.slice( 3, buyID.length ); // grabs true ID
	ShoppingArea.Basket.addItem( id, units );
	if( units == 0 ) {
		ShoppingArea.feedbackMessage( ( 'talk' + id ), 'neutral', units + ' item added', 1 );
	} else if( units == 1 ) {
		ShoppingArea.feedbackMessage( ( 'talk' + id ), 'positive', units + ' item added', 0.5 );	
	} else {
		ShoppingArea.feedbackMessage( ( 'talk' + id ), 'positive', units + ' items added', 0.5 );
	}
	ShoppingArea.saveCartToCookie();
	ShoppingArea.refreshAll();
}
ShoppingArea.addToCartSingle = function( id ) { //===Calls shopping basket's <addItem> for single unit of an item===
	ShoppingArea.Basket.addItem( id, 1 );
	ShoppingArea.saveCartToCookie();
	ShoppingArea.refreshAll();
}
ShoppingArea.removeFromCartSingle = function( id ) { //===Calls shopping basket's <removeItem> for single unit of an item===
	ShoppingArea.Basket.removeItem( id, 'single' );
	ShoppingArea.saveCartToCookie();
	ShoppingArea.refreshAll();
}
ShoppingArea.removeFromCartAll = function( id ) { //===Calls shopping basket's <removeItem> for all units of an item===
	ShoppingArea.Basket.removeItem( id, 'all' );
	ShoppingArea.saveCartToCookie();
	ShoppingArea.refreshAll();
}
ShoppingArea.clearCart = function() { //===Calls shopping basket's <removeAll> to clear everything from it===
	if( ShoppingArea.Basket.unitsTotal > 0 ) {
		ShoppingArea.Basket.removeAll();
		ShoppingArea.saveCartToCookie();
		ShoppingArea.refreshAll();
		ShoppingArea.feedbackMessage( 'controlFeedback', 'positive', 'All items cleared', 2 );
	} else {
		ShoppingArea.feedbackMessage( 'controlFeedback', 'neutral', 'Already empty', 2 );
	}
}
ShoppingArea.healthyOptionSwitch = function() { //===Turns On/Off the healthy option for the order===
	var  healthChk = document.getElementById( 'healthyOption' ).checked;
	//console.log( "FUNCTION <ShoppingArea.healthyOptionSwitch> switching to: " + healthChk );
	ShoppingArea.Basket.healthyOption = healthChk;
	ShoppingArea.saveCartToCookie();
}
ShoppingArea.applyShipping = function( selectValue ) { //===Calls shopping basket's <setShipping> for the cart===
	ShoppingArea.Basket.setShipping( selectValue );
	ShoppingArea.saveCartToCookie();
	ShoppingArea.refreshAll();
}
ShoppingArea.refreshAll = function() { //===Calls all display REFRESH function in ShoppingArea & saves to cookie===
	//console.log( "FUNCTION <ShoppingCart.refreshAll> in page " + ShoppingArea.currentPage );
	switch( ShoppingArea.currentPage ) {
		case 1:		ShoppingArea.refreshShoppingCart();
						ShoppingArea.refreshButton();
						break;
		case 2:		ShoppingArea.Basket.getShipping();
						ShoppingArea.refreshCheckoutCart();
						break;
		case 3:		ShoppingArea.Basket.getShipping();
						ShoppingArea.refreshCheckout2();
						break;
		case 4:		ShoppingArea.Basket.getShipping();
						ShoppingArea.refreshCheckout3();
						break;
		case 5:		ShoppingArea.Basket.getShipping();
						ShoppingArea.refreshCheckout4();
						break;
		default: 	//console.log( ">Page " + ShoppingArea.currentPage + " does not exist" );
						//console.log( "<ShoppingCart.refreshAll> page " +ShoppingArea.currentPage +" case does not exist:" );
	}
}
//==================================================================================
// DISPLAY/REFRESH FUNCTIONS FOR ALL AREAS
//==================================================================================
ShoppingArea.refreshShoppingCart = function() { //===Refreshes the ShoppingArea cart display===
	document.getElementById( "shpItemsInCart" ).innerHTML = '';
	document.getElementById( "shoppingCartTotals" ).innerHTML = '';
	if( ShoppingArea.Basket.content.length == 0 ) {
		document.getElementById( "ButtonContainerName" ).innerHTML = "<img src='images/basket.png' alt='Basket' width='30' height='30'>"
		document.getElementById( "shpItemsInCart" ).innerHTML = "<div class='shpItem'>No items in cart</div>";
	} else {
		document.getElementById( "ButtonContainerName" ).innerHTML = "<img src='images/basket_full.png' alt='Basket' width='30' height='30'>"
		for( var i = 0; i < ShoppingArea.Basket.content.length; ++i ) {
			document.getElementById( "shpItemsInCart" ).innerHTML += 
				"<div class='shpContainer'>" +
					"<div class='shpItem'>" + ShoppingArea.Basket.content[ i ].itsName + "</div>" + 
					"<div class='shpQuantity'><div><input class='minusButton' type='button' value='' onclick='ShoppingArea.removeFromCartSingle( \"" + ShoppingArea.Basket.content[ i ].itsId + "\" )' /></div><div>" +ShoppingArea.Basket.content[ i ].itsUnits + "</div><div><input class='plusButton' type='button' value='' onclick='ShoppingArea.addToCartSingle( \"" + ShoppingArea.Basket.content[ i ].itsId + "\" )' /></div></div>" + 
					"<div class='shpAmount'>£ " + ( ShoppingArea.Basket.content[ i ].itsTotal ).toFixed( 2 ) + "</div>" + 
					"<div class='shpRemove'><input class='removeButton' type='button' value='' onclick='ShoppingArea.removeFromCartAll( \"" + ShoppingArea.Basket.content[ i ].itsId + "\" )' /> </div>" +
				"</div>";	
		}
		document.getElementById( "shoppingCartTotals" ).innerHTML += 
			"<div class='shpTotalsH'>Items Total:</div><div class='shpTotals'>£ " + ( ShoppingArea.Basket.subTotal1 ).toFixed( 2 ) + "</div>" +
			"<div class='shpTotalsH'>includes V.A.T.:</div><div class='shpTotals'>£ " + ( ShoppingArea.Basket.taxTotal ).toFixed( 2 ) + "</div>";
		if( ShoppingArea.Basket.unitsTotal >= ShoppingArea.discountPoint ) {
			document.getElementById( "shoppingCartTotals" ).innerHTML +=
				"<div class='shpTotalsH'>Discount (12.5%):</div><div class='shpTotals'>£ " + ( ShoppingArea.Basket.discountTotal ).toFixed( 2 ) + "</div>";
		} else {
			document.getElementById( "shoppingCartTotals" ).innerHTML +=
				"<div class='shpTotalsH'>Discount (12.5%):</div><div class='shpTotals'>" + (  ShoppingArea.discountPoint - ShoppingArea.Basket.unitsTotal ) + "  more to qualify.</div>";
		}
		document.getElementById( "shoppingCartTotals" ).innerHTML +=
			"<div class='shpTotalsH'>Sub-Total:</div><div class='shpTotals'>£ " + ( ShoppingArea.Basket.subTotal2 ).toFixed( 2 ) + "</div>";
	}
} 
ShoppingArea.refreshButton = function() { //===Refreshes the ShoppingArea cart button info===
	if( ShoppingArea.Basket.unitsTotal > 0 ) {
		document.getElementById( "ButtonUnits" ).innerHTML = ShoppingArea.Basket.unitsTotal;
	} else {
		document.getElementById( "ButtonUnits" ).innerHTML = "none";
	}
	document.getElementById( "ButtonTotal" ).innerHTML = "£" + ( ShoppingArea.Basket.subTotal2 ).toFixed( 2 );
}
ShoppingArea.refreshCheckoutCart = function() { //===Refreshes the Shopping Basket/Checkout 1 display===
	document.getElementById( "chkItemsInCart" ).innerHTML = '';
	document.getElementById( "checkoutCartTotals" ).innerHTML = '';
	if( ShoppingArea.Basket.content.length == 0 ) {
		document.getElementById( "chkItemsInCart" ).innerHTML = "<div class='chkItem'>No items in cart</div>";
	} else {
		for( var i = 0; i < ShoppingArea.Basket.content.length; ++i ) {
			document.getElementById( "chkItemsInCart" ).innerHTML += 
				"<div class='chkContainer'>" +
					"<div class='chkCode'>" + ShoppingArea.Basket.content[ i ].itsId + "</div>" +
					"<div class='chkItem'>" + ShoppingArea.Basket.content[ i ].itsName + "</div>" +
					"<div class='chkPrice'>£ " + ShoppingArea.Basket.content[ i ].itsPrice + "</div>" +
					"<div class='chkQuantity'><div><input class='minusButton' type='button' value='' onclick='ShoppingArea.removeFromCartSingle( \"" + ShoppingArea.Basket.content[ i ].itsId + "\" )' /></div><div>" +ShoppingArea.Basket.content[ i ].itsUnits + "</div><div><input class='plusButton' type='button' value='' onclick='ShoppingArea.addToCartSingle( \"" + ShoppingArea.Basket.content[ i ].itsId + "\" )' /></div></div>" +
					"<div class='chkAmount'>£ " + ( ShoppingArea.Basket.content[ i ].itsTotal ).toFixed( 2 ) + "</div>" +
					"<div class='chkRemove'><input class='removeButton' type='button' value='' onclick='ShoppingArea.removeFromCartAll( \"" + ShoppingArea.Basket.content[ i ].itsId + "\" )' /> </div>" +
				"</div>";	
		}
		document.getElementById( "checkoutCartTotals" ).innerHTML += 
			"<div class='chkTotalsH'>Items Total:</div><div class='chkTotals'>£ " + ( ShoppingArea.Basket.subTotal1 ).toFixed( 2 ) + "</div><div class='infoHolder'></div>" +
			"<div class='infoHolder'><input class='infoButton' type='button' value='' onmouseover='showInfo( event, \"" +'infoItemTotal' +"\" )' onmouseout='showInfo( event, \"" + 'infoItemTotal' + "\" )'/></div>" +
			"<div class='chkTotalsH'>includes V.A.T.:</div><div class='chkTotals'>£ " + ( ShoppingArea.Basket.taxTotal ).toFixed( 2 ) + "</div><div class='infoHolder'></div>" + 
			"<div class='infoHolder'><input class='infoButton' type='button' value='' onmouseover='showInfo( event, \"" +'infoVAT' +"\" )' onmouseout='showInfo( event, \"" + 'infoVAT' + "\" )'/></div>";
		if( ShoppingArea.Basket.unitsTotal >= ShoppingArea.discountPoint ) {
			document.getElementById( "checkoutCartTotals" ).innerHTML +=
				"<div class='chkTotalsH'>Discount (12.5%):</div><div class='chkTotals'>£ " + ( ShoppingArea.Basket.discountTotal ).toFixed( 2 ) + "</div><div class='infoHolder'></div>" + 
				"<div class='infoHolder'><input class='infoButton' type='button' value='' onmouseover='showInfo( event, \"" +'infoDiscount' +"\" )' onmouseout='showInfo( event, \"" + 'infoDiscount' + "\" )'/></div>";
		} else {
			document.getElementById( "checkoutCartTotals" ).innerHTML +=
				"<div class='chkTotalsH'>Discount (12.5%):</div><div class='chkTotals'>" + (  ShoppingArea.discountPoint - ShoppingArea.Basket.unitsTotal ) + "  more to qualify.</div>" + 
				"<div class='infoHolder'><input class='infoButton' type='button' value='' onmouseover='showInfo( event, \"" +'infoDiscount' +"\" )' onmouseout='showInfo( event, \"" + 'infoDiscount' + "\" )'/></div>";
		}
		document.getElementById( "checkoutCartTotals" ).innerHTML +=
			"<div class='chkTotalsH'>Sub-Total:</div><div class='chkTotals'>£ " + ( ShoppingArea.Basket.subTotal2 ).toFixed( 2 ) + "</div>" +
			"<div class='infoHolder'><input class='infoButton' type='button' value='' onmouseover='showInfo( event, \"" +'infoSubTotal' +"\" )' onmouseout='showInfo( event, \"" + 'infoSubTotal' + "\" )'/></div>";
		document.getElementById( "checkoutCartTotals" ).innerHTML +=
			"<div class='chkTotalsH'>Shipping:</div><div class='chkTotals'>£ " + ( ShoppingArea.Basket.shippingCost ).toFixed( 2 ) + "</div>" +
			"<div class='infoHolder'><input class='infoButton' type='button' value='' onmouseover='showInfo( event, \"" +'infoShipping' +"\" )' onmouseout='showInfo( event, \"" + 'infoShipping' + "\" )'/></div>" +
			"<div class='right'><select id='shippingOptions' onchange='ShoppingArea.applyShipping( this.value );'><option value='0'>Select shipping</option></select>" +
			"<div class='right' id='errorShipping'></div></div>";
		document.getElementById( "checkoutCartTotals" ).innerHTML +=
			"<div class='chkTotalsH'>Total:</div><div class='chkTotals'>£ " + ( ShoppingArea.Basket.total ).toFixed( 2 ) + "</div><div class='infoHolder'></div>" +
			"<div class='infoHolder'><input class='infoButton' type='button' value='' onmouseover='showInfo( event, \"" +'infoTotal' +"\" )' onmouseout='showInfo( event, \"" + 'infoTotal' + "\" )'/></div>";
		ShoppingArea.addShippingOptions();
		document.getElementById( 'healthyOption' ).checked = ShoppingArea.Basket.healthyOption;
		if( ShoppingArea.Basket.shippingSelected > 0 ) {
			document.getElementById( "shippingOptions" ).value = ShoppingArea.Basket.shippingSelected;
			document.getElementById( 'errorShipping' ).innerHTML = "<img src='images/validated.png' alt='OK' height='18' width='18'>";
			document.getElementById( 'controlFeedback' ).innerHTML = '';
		}
	}
}
ShoppingArea.refreshCheckout2 = function() {
	//console.log( "FUNCTION <ShoppingArea.refreshCheckout2>" );
	var billingFields = new Array( 'titleSelectorB', 'nameB', 'surnameB', 'address1B', 'address2B', 'postcodeB', 'cityB', 'countrySelectorB' );
	var shippingFields = new Array( 'titleSelectorS', 'nameS', 'surnameS', 'address1S', 'address2S', 'postcodeS', 'cityS', 'countrySelectorS' );
	for( var i = 0; i < billingFields.length; ++i ) {
		document.getElementById( billingFields[ i ] ).value = ShoppingArea.Buyer.billingAddress[ i ];
	}
	if( ShoppingArea.Buyer.sameAddress == true ) {
		document.getElementById( 'sameAddress' ).checked = true;
		doubleAddress();
	} else {
		for( var i = 0; i < shippingFields.length; ++i ) {
			document.getElementById( shippingFields[ i ] ).value = ShoppingArea.Buyer.shippingAddress[ i ];
		}
	}
	document.getElementById( 'phone' ).value = ShoppingArea.Buyer.phone;
	document.getElementById( 'email' ).value = ShoppingArea.Buyer.email;
}
ShoppingArea.refreshCheckout3 = function() {
	//console.log( "FUNCTION <ShoppingArea.refreshCheckout3>" );
	document.getElementById( 'chk3Basket' ).innerHTML = "<h4>Your Basket:</h4>";
	for( var i = 0; i < ShoppingArea.Basket.content.length; ++i ) {
		document.getElementById( 'chk3Basket' ).innerHTML += 
			ShoppingArea.Basket.content[ i ].itsUnits + " x "  +
			ShoppingArea.Basket.content[ i ].itsName + "</br>";
	}
	if( ShoppingArea.Basket.healthyOption == true ) {
		document.getElementById( 'chk3Basket' ).innerHTML += "</br>Healthy option selected.";
	} else {
		document.getElementById( 'chk3Basket' ).innerHTML += "</br>Healthy option not selected.";
	}
	var titles = new Array( 'Mr', 'Miss', 'Mrs' );
	document.getElementById( 'chk3CustomerBilling' ).innerHTML = "<h4>Bill to:</h4>";
	for( var i = 0; i < ShoppingArea.Buyer.billingAddress.length; ++i ) {
		if( i == 0 ) {
			document.getElementById( 'chk3CustomerBilling' ).innerHTML += titles[ ShoppingArea.Buyer.billingAddress[ i ] - 1 ];
		} else if( i < 3 ) {
			document.getElementById( 'chk3CustomerBilling' ).innerHTML += " " + ShoppingArea.Buyer.billingAddress[ i ];
		} else if( i == 7 ) {
			document.getElementById( 'chk3CustomerBilling' ).innerHTML +=  ", " + shipping.destinations[ ShoppingArea.Buyer.billingAddress[ i ] - 1 ].country;
		} else {
			document.getElementById( 'chk3CustomerBilling' ).innerHTML +=  "</br>" + ShoppingArea.Buyer.billingAddress[ i ];
		}
	}
	document.getElementById( 'chk3CustomerBilling' ).innerHTML += "</br></br>Phone: " + ShoppingArea.Buyer.phone;
	document.getElementById( 'chk3CustomerBilling' ).innerHTML += "</br>e-Mail: " + ShoppingArea.Buyer.email;
	document.getElementById( 'chk3CustomerShipping' ).innerHTML = "<h4>Ship to:</h4>";
	document.getElementById( 'chk3ShipBy' ).innerHTML = "<h4>Shipping Method:</h4>" + shipping.options[ ShoppingArea.Basket.shippingSelected - 1 ].name;
	for( var i = 0; i < ShoppingArea.Buyer.shippingAddress.length; ++i ) {
		if( i == 0 ) {
			document.getElementById( 'chk3CustomerShipping' ).innerHTML += titles[ ShoppingArea.Buyer.shippingAddress[ i ] - 1 ];
		} else if( i < 3 ) {
			document.getElementById( 'chk3CustomerShipping' ).innerHTML += " " + ShoppingArea.Buyer.shippingAddress[ i ];
		} else if( i == 7 ) {
			document.getElementById( 'chk3CustomerShipping' ).innerHTML += ", " + shipping.destinations[ ShoppingArea.Buyer.shippingAddress[ i ] - 1 ].country;
		} else {
			document.getElementById( 'chk3CustomerShipping' ).innerHTML +=   "</br>" + ShoppingArea.Buyer.shippingAddress[ i ];
		}
	}
	document.getElementById( 'chk3Totals' ).innerHTML = "<h4>Totals:</h4>"
	document.getElementById( 'chk3Totals' ).innerHTML += "Inventory total: £" + ( ShoppingArea.Basket.subTotal1 ).toFixed( 2 );
	document.getElementById( 'chk3Totals' ).innerHTML += "</br>" +"V.A.T. included: £" + ( ShoppingArea.Basket.taxTotal ).toFixed( 2 );
	document.getElementById( 'chk3Totals' ).innerHTML += "</br>" +"Discount applied: £" + ( ShoppingArea.Basket.discountTotal ).toFixed( 2 );
	document.getElementById( 'chk3Totals' ).innerHTML += "</br>" +"Shipping: £" + ( ShoppingArea.Basket.shippingCost ).toFixed( 2 );
	document.getElementById( 'chk3Totals' ).innerHTML += "</br></br><strong>" +"Total to pay: £" + ( ShoppingArea.Basket.total ).toFixed( 2 ) + "</strong>";

}
ShoppingArea.refreshCheckout4 = function() {
	document.getElementById( 'chk4Total' ).innerHTML = ( ShoppingArea.Basket.total ).toFixed( 2 );
	ShoppingArea.addSelectFieldsCHK4();
}
//==================================================================================
//  'SELECT' HTML ITEM POPULATION MODULE (FOR 'SHIPPING OPTION', 'TITLE', 'COUNTRY')
//==================================================================================
ShoppingArea.addShippingOptions = function() {
	var price;
	for( var i = 0; i < shipping.options.length; ++i ) {
		if( ShoppingArea.Basket.unitsTotal >= shipping.options[ i ].discountPoint ) {
			if(  shipping.options[ i ].discountPrice == 0 ) {
				price = 'Free';
			} else {
				price = "£" + shipping.options[ i ].discountPrice;
			}
		} else {
			price = "£" + shipping.options[ i ].stdPrice;
		}
		addField( document.getElementById( 'shippingOptions' ), shipping.options[ i ].name + " (" + price + ")", i );
	}
}
ShoppingArea.addSelectFieldsCHK2 = function() {
	var shipDestDefault = 0;
	var titles = new Array( 'Mr', 'Miss', 'Mrs' );
	for( var i = 0; i < shipping.destinations.length; ++i ) {
		addField( document.getElementById( 'countrySelectorB' ), shipping.destinations[ i ].country, i ); 
		addField( document.getElementById( 'countrySelectorS' ), shipping.destinations[ i ].country, i ); 
	}
	for( var i = 0; i < titles.length; ++i ) {
		addField( document.getElementById( 'titleSelectorB' ), titles[ i ], i );
		addField( document.getElementById( 'titleSelectorS' ), titles[ i ], i );
	}
}
ShoppingArea.addSelectFieldsCHK4 = function() {
	var date = new Date();
	var year = date.getFullYear();
	for( var i = 0; i < 12; ++i ) { //for the months
		addField( document.getElementById( 'CCExpireMonth' ), ( i + 1 ), i );
	}
	for( var i = 0; i < 20; ++i ) { //for the years
		addField( document.getElementById( 'CCExpireYear' ), ( year + i ), i );	
	}		
}
function addField( select_box, text, value ) {
		var field = document.createElement( 'option' );
		field.text = text;
		field.value = value + 1;
		select_box.options.add( field );
}
//==================================================================================
//  COOKIE HANDLING
//==================================================================================
function createCookie( name, value ) {
	//console.log( "FUNCTION <createCookie> : " + name );
	document.cookie = name + "=" + value + name + "_END";
}
function readCookie( name ) {
	//console.log( "FUNCTION <readCookie> : " + name);
	if( document.cookie.indexOf( name ) != -1 ) {
		var rawCookie =  decodeURIComponent( document.cookie );
		var from = rawCookie.indexOf( name ) + ( name + "=" ).length;
		var to = rawCookie.indexOf( name + "_END" );
		var bakedCookie = rawCookie.substring( from, to);
		return bakedCookie;
	} else {
		return 'invalid';
	}
}
function checkCookie() {
	//console.log( 'Cookies enabled: ' +  navigator.cookieEnabled );
	if( navigator.cookieEnabled == true ) {
		return true;
	} else {
		return false;
	}
}
function killCookie( name ) {
	//console.log( "FUNCTION <killCookie> : " + name );
	document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
//==================================================================================
//  MESSAGE FEEDBACK, INFO BUBBLES FUNCTIONS AND MISC
//==================================================================================
ShoppingArea.feedbackMessage = function( location, type, message, n ) { //===Displays a message for 'n' secs===
		//console.log( "FUNCTION <ShoppingArea.feedbackMessage>");
		var mseconds = n * 1000;
		var shout = document.getElementById( location );
		shout.className = type;
		shout.innerHTML = message;
		setTimeout( function() { shout.innerHTML = '' }, mseconds );
}
function showInfo( pointer, divId ) { //===Shows/Hides a Div @ location of cursor===
    var left  = pointer.clientX  + "px";
    var top  = pointer.clientY  + "px";
    document.getElementById( divId ).style.left = left;
    document.getElementById( divId ).style.top = top;
	if( document.getElementById( divId ).className == 'hideBubble' ) {
		document.getElementById( divId ).className = 'showBubble';
	} else {
		document.getElementById( divId ).className = 'hideBubble'
	}
}
function browseBack() { //===Browser histroy back button call===
	window.history.back();
}
//==================================================================================
//  ERROR HANDLING FUNCTIONS
//==================================================================================
function browserCheck() { //===Checks for supported browsers===
	var check = false;
	var supportedRegExp = /Firefox|MSIE|Chrome/;
	var versionRegExp = /[0-9]*\.?[0-9]+?/;
	var usrAgt = navigator.userAgent;
	if( supportedRegExp.test( usrAgt ) == true ) {
		var browser = usrAgt.match( supportedRegExp );
		var usrAgtEnd = usrAgt.slice( browser.index );
		var version = parseFloat( usrAgtEnd.match( versionRegExp ) );
		if( browser == 'Firefox' && version >= 12.0 ) {
			check = true;
		} else if( browser == 'MSIE' && version >= 8.0 ) {
			check = true;
		} else if( browser == 'Chrome' && version >= 22.0 ) {
			check = true;
		}
		//console.log( 'Browser: ' + browser + ' ' + version + ', Supported: ' + check );
	}
	return check;
}
ShoppingArea.validatePage = function() { //===Checks or Calls the page specific functions to validate=== 
	//console.log( "FUNCTION <ShoppingCart.validatePage> in page " + ShoppingArea.currentPage );
	switch( ShoppingArea.currentPage ) {
		case 1:		if( !( ShoppingArea.Basket.unitsTotal > 0 ) ) {
							ShoppingArea.feedbackMessage( 'controlFeedback', 'negative', 'Basket is empty', 2 );
							return false;
						} else {
							return true;
						}		
		case 2:		if( !( ShoppingArea.Basket.unitsTotal > 0 ) ) {
							ShoppingArea.feedbackMessage( 'controlFeedback', 'negative', 'Your Basket is empty', 5 );
							return false;
						} else if( !( ShoppingArea.Basket.shippingSelected > 0 ) ) {
							document.getElementById( 'errorShipping' ).innerHTML = "<img src='images/error.png' alt='OK' height='18' width='18'>";
							ShoppingArea.feedbackMessage( 'controlFeedback', 'negative', 'You need to select a shipping method', 5 );
							return false;
						} else {
							return true;
						}
		case 3:		if( ShoppingArea.validateInputsCHK2() == false ) {
							ShoppingArea.feedbackMessage( 'controlFeedback', 'negative', 'Details incomplete or has errors', 5 );
							return false;
						} else {
							return true;
						}
		case 4:		return true;
		case 5:		if( ShoppingArea.validateInputsCHK4() == false ) {
							ShoppingArea.feedbackMessage( 'controlFeedback', 'negative', 'Invalid payment details', 5 );
							return false;
						} else {
							return true;
						}
		default: 	//console.log( ">ERROR @ <ShoppingCart.validatePage>" + ShoppingArea.currentPage );
						//console.log( "<ShoppingCart.validatePage> Page " + ShoppingArea.currentPage + " case doesn't exist" );
	}
}
ShoppingArea.validateInputsCHK2 = function() { //===checks all fields in address page (chk2)===
	//console.log( "FUNCTION <ShoppingArea.validateInputsCHK2>" );
	var errorFieldID;
	var validates = true;
	var selectors = new Array( 'titleSelectorS', 'titleSelectorB', 'countrySelectorB', 'countrySelectorS' );
	for( var i = 0; i < selectors.length; ++i ) {
		var errorFieldID = selectors[ i ] + 'Error';
		if( document.getElementById( selectors[ i ] ).value == 0 ) {
			document.getElementById( errorFieldID ).innerHTML = "<img src='images/error.png' alt='OK' height='18' width='18'>";
			validates = false;
		} else {
			document.getElementById( errorFieldID ).innerHTML = "";
		}
	}
	var pureTextFields = new Array( 'nameB', 'nameS', 'surnameB', 'surnameS', 'cityB', 'cityS' );
	var pureTextRegExp = /^(?=[a-zA-Z]{2,})[a-zA-Z\-\s\']{2,}$/;
	for( var i = 0; i < pureTextFields.length; ++i ) {
		pureTextRegExp.lastIndex = 0;
		errorFieldID = pureTextFields[ i ] + 'Error';
		if( pureTextRegExp.test( document.getElementById( pureTextFields[ i ] ).value ) == false ) {
			document.getElementById( errorFieldID ).innerHTML = "<img src='images/error.png' alt='OK' height='18' width='18'>";
			validates = false;
		} else {
			document.getElementById( errorFieldID ).innerHTML = "";
		}
	}
	var mixedTextFields1 = new Array( 'address1B', 'address1S' );
	var mixedTextRegExp1 = /^(?=[a-zA-Z0-9]{1,})([0-9a-zA-Z-',\s]{5,})$/;
	for( var i = 0; i < mixedTextFields1.length; ++i ) {
		mixedTextRegExp1.lastIndex = 0;
		errorFieldID = mixedTextFields1[ i ] + 'Error';
		if( mixedTextRegExp1.test( document.getElementById( mixedTextFields1[ i ] ).value ) == false ) {
			document.getElementById( errorFieldID ).innerHTML = "<img src='images/error.png' alt='OK' height='18' width='18'>";
			validates = false;
		} else {
			document.getElementById( errorFieldID ).innerHTML = "";
		}
	}
	var mixedTextFields2 = new Array( 'address2B', 'address2S' );
	var mixedTextRegExp2 = /^(?=[a-zA-Z0-9]{1,})([0-9a-zA-Z-',\s]{5,})$/;
	for( var i = 0; i < mixedTextFields2.length; ++i ) {
		mixedTextRegExp2.lastIndex = 0;
		errorFieldID = mixedTextFields2[ i ] + 'Error';
		if( mixedTextRegExp2.test( document.getElementById( mixedTextFields2[ i ] ).value ) == false ) {
			document.getElementById( errorFieldID ).innerHTML = "<img src='images/error.png' alt='OK' height='18' width='18'>";
			validates = false;
		} else {
			document.getElementById( errorFieldID ).innerHTML = "";
		}
	}
	var postcodeFields = new Array( 'postcodeB', 'postcodeS' );
	var postcodeRegExp = /^([A-Za-z]{1,2})([0-9]{2,3})([A-Za-z]{2})$|^([A-Za-z]{1,2})([0-9]{1})([A-Za-z]{1})([0-9]{1})([A-Za-z]{2})$/;
	for( var i = 0; i < postcodeFields.length; ++i ) {
		postcodeRegExp.lastIndex= 0;
		errorFieldID = postcodeFields[ i ] + 'Error';
		if( postcodeRegExp.test( document.getElementById( postcodeFields[ i ] ).value ) == false ) {
			document.getElementById( errorFieldID ).innerHTML = "<img src='images/error.png' alt='OK' height='18' width='18'>";
			validates = false;
		} else {
			document.getElementById( errorFieldID ).innerHTML = "";
		}					
	}	
	var emailField = 'email';
	errorFieldID = emailField + 'Error';
	var emailRegExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if( emailRegExp.test( document.getElementById( emailField ).value ) == false ) {
		document.getElementById( errorFieldID ).innerHTML = "<img src='images/error.png' alt='OK' height='18' width='18'>";
		validates = false;
	} else {
		document.getElementById( errorFieldID ).innerHTML = "";
	}							
	var telephoneField = 'phone'
	errorFieldID = telephoneField + 'Error';
	var telephoneRegExp = /^([0-9]{11})$/;
	if( telephoneRegExp.test( document.getElementById( telephoneField ).value ) == false ) {
		document.getElementById( errorFieldID ).innerHTML = "<img src='images/error.png' alt='OK' height='18' width='18'>";
		validates = false;
	} else {
		document.getElementById( errorFieldID ).innerHTML = "";
	}
	return validates;
}
ShoppingArea.validateInputsCHK4 = function() { //===checks all fields in payment page (chk3)===
	//console.log( "FUNCTION <ShoppingArea.validateInputsCHK3>" );
	var validates = true;
	var CCNameRegExp = /^(?=[A-Za-z]{3,})([A-Za-z\’\-\s]{3,})$/;
	if( CCNameRegExp.test( document.getElementById( 'CCName' ).value ) == false ) {
		document.getElementById( 'CCNameError' ).innerHTML = "<img src='images/error.png' alt='OK' height='18' width='18'>";
		validates = false;
	} else {
		document.getElementById( 'CCNameError' ).innerHTML = "";
	}
	
	var CCNumberRegExp = /^[0-9]{16}$/;
	if( CCNumberRegExp.test( document.getElementById( 'CCNumber' ).value ) == false ) {
		document.getElementById( 'CCNumberError' ).innerHTML = "<img src='images/error.png' alt='OK' height='18' width='18'>";
		validates = false;
	} else {
		document.getElementById( 'CCNumberError' ).innerHTML = "";
	}
	var cc_month = document.getElementById( 'CCExpireMonth' ).options[ document.getElementById( 'CCExpireMonth' ).selectedIndex ].text;
	var cc_year = document.getElementById( 'CCExpireYear' ).options[ document.getElementById( 'CCExpireYear' ).selectedIndex ].text;
	var date = new Date();
	var month = date.getMonth() + 1;
	var year = date.getFullYear();
	if( document.getElementById( 'CCExpireMonth' ).value == 0 || document.getElementById( 'CCExpireYear' ).value == 0 ) {
		document.getElementById( 'CCExpireError' ).innerHTML = "<img src='images/error.png' alt='OK' height='18' width='18'>";
		validates = false;
	} else if( cc_year == year && cc_month < month ) {
		document.getElementById( 'CCExpireError' ).innerHTML = "<img src='images/error.png' alt='OK' height='18' width='18'>";
		validates = false;
	} else { 
		document.getElementById( 'CCExpireError' ).innerHTML = "";
	}
	var CCSecCodeRegExp = /^[0-9]{3}$/;
	if( CCSecCodeRegExp.test( document.getElementById( 'CCSecCode' ).value ) == false ) {
		document.getElementById( 'CCSecCodeError' ).innerHTML = "<img src='images/error.png' alt='OK' height='18' width='18'>";
		validates = false;
	} else {
		document.getElementById( 'CCSecCodeError' ).innerHTML = "";
	}
	return validates;
}
//==================================================================================
// JSON INVENTORY DATA, JSON SHIPPING DATA
//==================================================================================
var shipping = {
	"options": [
	  {
		"name": "Royal Mail Package",
		"zone": "1",
		"stdPrice": "5.99",
		"discountPoint": "5",
		"discountPrice": "0"
	  },
	  {
		"name": "Courier",
		"zone": "1",
		"stdPrice": "10.00",
		"discountPoint": "5",
		"discountPrice": "4.99"
	  }
	],
	"defaultDestination": "1",
	"destinations": [
	  {
		"country": "United Kingdom",
		"zone": "1",
	  }
	]
}
var inventory = {
    "item": [
      {
		"category": "juice",
		"id": "J01",
		"name": "Classic OJ",
		"price": "1.99",
		"description": "Classic orange juice with no concentrates",
		"image": "J01.jpg"
      },
      {
        "category": "juice",
        "id": "J02",
        "name": "Blood Orange",
        "price": "2.29",
        "description": "Juice made with blood oranges. No concentrates, with pulp.",
        "image": "J02.jpg"
      },
      {
        "category": "juice",
        "id": "J03",
        "name": "Grapefruit",
        "price": "2.50",
        "description": "100% delicious grapefruit juice for your morning breakfast.",
        "image": "J03.jpg"
      },
      {
        "category": "juice",
        "id": "J04",
        "name": "Pomegranate Juice",
        "price": "4.99",
        "description": "100% pure pomegranate juice. Not made with concentrates",
        "image": "J04.jpg"
      },
      {
        "category": "juice",
        "id": "J05",
        "name": "Tomatoe Juice",
        "price": "2.79",
        "description": "Whole tomatoe juice with no additives.",
        "image": "J05.jpg"
      },
      {
        "category": "smoothie",
        "id": "S01",
        "name": "Classic red Smoothie",
        "price": "2.99",
        "description": "Strawberries and Bananas",
        "image": "S01.jpg"
      },
      {
        "category": "smoothie",
        "id": "S02",
        "name": "Berry Banana Flaxseed Protein Smoothie",
        "price": "4.49",
        "description": "Blueberries, Raspberries, Strawberries, Banana, Almond milk, Flax seeds, Vanilla whey protein powder",
        "image": "S02.jpg"
      },
      {
        "category": "smoothie",
        "id": "S03",
        "name": "Kiwi-Blueberry-Pomegranate Protein Smoothie",
        "price": "4.99",
        "description": "Flax Milk, Greek Yogurt, Kiwis, Blueberries, Pomegranate, Ground Flax Seeds, Drizzle of Agave Nectar",
        "image": "S03.jpg"
      },
      {
        "category": "smoothie",
        "id": "S04",
        "name": "Superfood Smoothie",
        "price": "4.99",
        "description": "berrie mix, coconut milk, coconut oil, soaked chia seeds, green superfood powder, vanilla protein powder",
        "image": "S04.jpg"
      },
      {
        "category": "smoothie",
        "id": "S05",
        "name": "Soya passion",
        "price": "3.49",
        "description": "Strawberries, Raspberries, Mango, Lemon and lime, Soya milk",
        "image": "S05.jpg"
      }
    ]
}