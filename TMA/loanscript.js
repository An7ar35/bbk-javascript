/*
=======================================================================
Name: Loan Calculator script
Author: E. A. Davison, 2013
-----------------------------------------------------------------------------------------------------
**NOTE**
underscored names are place holders in the web page (e.g.: loan_amount )
camelcased names are js variables (e.g.: loanAmount )
=======================================================================
*/
// Main function to manage and calculate everything
function calculateLoan() {
	var loanAmount = document.getElementById( 'loan_amount' ).value;
	var repayPeriod = document.getElementById( 'repay_period' ).value;
	//Error handling
	if( loanAmount < 1000 || repayPeriod < 1 || repayPeriod > 25 ) {
		var errorMessage = 'ERROR\n___________________________________________________________\n\n';
		if( loanAmount < 1000 ) {
			document.getElementById( "loan_amount" ).style.backgroundColor = '#FFCCD9';
			errorMessage += '> Invalid loan amount:  minimum is 1000\n';
		}
		if( repayPeriod < 1 || repayPeriod > 25 ) {
			document.getElementById( "repay_period" ).style.backgroundColor = '#FFCCD9';
			errorMessage += '> Invalid repayment period: should be between 1 and 25 years inclusive';
		}
		alert( errorMessage );
	} else {		//Calculations, printing
		resetColours();
		blockInput( true );
		var interestRate = getRate( loanAmount );
		var interestTotal = loanAmount * interestRate * repayPeriod;
		var protectAmount = 0;
		if( document.getElementById( "protect_box" ).checked == true ) {
			protectAmount =  loanAmount * 0.05;
			document.getElementById( "protect_amount" ).innerHTML = "&#163; " + protectAmount;
		} else {
			document.getElementById( "protect_amount" ).innerHTML = "N/A";
		}
		var totalRepay = parseInt( loanAmount ) + parseInt( interestTotal ) + parseInt( protectAmount );
		var monthlyPayment = totalRepay / ( repayPeriod * 12 );
		document.getElementById( "interest_rate" ).innerHTML = ( interestRate * 100 ).toFixed( 2 ) + "% per anum";
		document.getElementById( "interest" ).innerHTML = "&#163; " + interestTotal.toFixed( 2 );
		document.getElementById( "total_repay" ).innerHTML = "&#163; " + totalRepay.toFixed( 2 );
		document.getElementById( "monthly_payment" ).innerHTML = "&#163; " + monthlyPayment.toFixed( 2 ) + " p/m";
	} 
}
// Calculate interest rate for loan -> called from calculateLoan()
function getRate( loanAmount ) {
	var baseRate = 0.0425;
	var interestRate = baseRate;
	if( loanAmount >= 1000 && loanAmount <=10000 ) {
		interestRate += 0.055;
	} else if( loanAmount > 10000 && loanAmount <=50000 ) {
		interestRate += 0.045;
	} else if( loanAmount > 50000 && loanAmount <= 100000 ) {
		interestRate += 0.035;
	} else if( loanAmount > 100000 ) {	
		interestRate += 0.025;
	}
	return interestRate;
} 
// Clears all fields in form (input & output)
function resetForms() {
	document.getElementById( "protect_amount" ).innerHTML = '';
	document.getElementById( "interest" ).innerHTML = '';
	document.getElementById( "interest_rate" ).innerHTML = '';
	document.getElementById( "total_repay" ).innerHTML = '';
	document.getElementById( "monthly_payment" ).innerHTML = '';
	resetColours();
	blockInput( false );
}
// Resets the input fields colours
function resetColours() {
	document.getElementById( "loan_amount" ).style.backgroundColor = '#FFFFFF';
	document.getElementById( "repay_period" ).style.backgroundColor = '#FFFFFF';
}
// Blocks/Unblocks the input fields (except checkbox: protect_box)
function blockInput( request ) {
	if( request == true ) {
		document.getElementById( "loan_amount" ).disabled = true;
		document.getElementById( "repay_period" ).disabled = true;
	} else {
		document.getElementById( "loan_amount" ).disabled = false;
		document.getElementById( "repay_period" ).disabled = false;	
	}
}
// "onblur" validation for both input fields for on-the-fly visual feedback [FAIL->red, PASS->white]
function checkValue( sourceID ) {
	if( sourceID == 'loan_amount' ) {
		if( document.getElementById( "loan_amount" ).value < 1000 ) {
			document.getElementById( "loan_amount" ).style.backgroundColor = '#FFCCD9';
		}
		else {
			document.getElementById( "loan_amount" ).style.backgroundColor = '#FFFFFF';
		}
	} 
	if ( sourceID == 'repay_period' ) {
		if( document.getElementById( "repay_period" ).value < 1 || document.getElementById( "repay_period" ).value > 25 ) {
			document.getElementById( "repay_period" ).style.backgroundColor = '#FFCCD9';
		}
		else {
			document.getElementById( "repay_period" ).style.backgroundColor = '#FFFFFF';
		}
	}
}